package com.pdf.demo.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther:
 * @Date: 2019/12/06/9:26
 * @Description:
 */
public class RunMain {

    public static void main(String[] args) throws Exception {

        //		这里是选择某个模板
        String templateName = "temp";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        SetDataUtil.setTempData(paramMap);

        //调用具体的实现方法
        PdfUtil.contractHandler(templateName, paramMap);

    }

}
